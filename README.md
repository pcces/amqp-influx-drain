# AMQP Influx Drain

Drain messages from an AMQP provider to an InfluxDB database.


# Usage

`docker run -it pcces/amqp-influx-drain:latest`


# Environment Variables

| Variable             | Default Value                                    | Description                                            |
| -------------------- | ------------------------------------------------ | ------------------------------------------------------ |
| MQ_DSN               | `amqp://guest:guest@localhost:5672/`             | The DSN connection string for the message queue        |
| MQ_CONNECT_TIMEOUT   | `2.0`                                            | The MQ connection timeout interval in seconds          |
| MQ_EXCHANGE_NAME     | `telegraf`                                       | The name of the message queue exchange to use          |
| MQ_EXCHANGE_TYPE     | `topic`                                          | The type of the message queue exchange                 |
| MQ_QUEUE_NAME        | `influxdb`                                       | The name of the queue to drain                         |
| MQ_QUEUE_ROUTING_KEY | `<empty string>`                                 | The message routing key to use                         |
| MQ_DRAIN_TIMEOUT     | `2.0`                                            | The interval in seconds until an event drain times out |
| MQ_ACK               | `true`                                           | Set to `true` to acknowledge handled messages          |
| INFLUX_DSN           | `influxdb://admin:admin@localhost:8086/telegraf` | The DSN connection string for the Influx database      |
| INFLUX_DB            | `telegraf`                                       | The name of the Influx database to push records into   |
| LOG_LEVEL            | `INFO`                                           | The log level. `DEBUG`, `INFO`, `WARNING`, or `ERROR`  |



# License

See the `LICENSE` file for details.
