#!/usr/bin/env python3

#
# amqp-influx-drain
#
# Drain messages from an AMQP provider to an InfluxDB database.
#
# Copyright © 2019 Daniel 'Vector' Kerr <dan@pcc.es>
# Copyright © 2019 PCC Event Services <support@pcc.es>
#
# Released under the MIT License.
# See the `LICENSE` file for details.
#

import influxdb
import kombu
import logging
import os
import socket
import time

from distutils.util import strtobool


def configure_logging():
  log_level = getattr( logging, os.getenv( 'LOG_LEVEL', 'INFO' ).upper() )
  log_format = '%(asctime)s %(levelname)s %(name)s %(message)s'
  logging.basicConfig( level = log_level, format = log_format )


def get_config():
  return {
    'MQ_DSN': os.getenv( 'MQ_DSN', 'amqp://guest:guest@localhost:5672/' ),
    'MQ_CONNECT_TIMEOUT': float( os.getenv( 'MQ_CONNECT_TIMEOUT', '2.0' ) ),
    'MQ_EXCHANGE_NAME': os.getenv( 'MQ_EXCHANGE_NAME', 'telegraf' ),
    'MQ_EXCHANGE_TYPE': os.getenv( 'MQ_EXCHANGE_TYPE', 'topic' ),
    'MQ_QUEUE_NAME': os.getenv( 'MQ_QUEUE_NAME', 'influxb' ),
    'MQ_QUEUE_ROUTING_KEY': os.getenv( 'MQ_QUEUE_ROUTING_KEY', '' ),
    'MQ_DRAIN_TIMEOUT': float( os.getenv( 'MQ_DRAION_TIMEOUT', '2.0' ) ),
    'MQ_ACK': strtobool( os.getenv( 'MQ_ACK', 'true' ) ) == 1,
    'INFLUX_DSN': os.getenv( 'INFLUX_DSN', 'influxdb://admin:admin@localhost:8086/telegraf' ),
    'INFLUX_DB': os.getenv( 'INFLUX_DB', 'telegraf' ),
  }


def get_influx( config ):
  client = influxdb.InfluxDBClient.from_dsn( config[ 'INFLUX_DSN' ] )
  return client


def get_amqp( config, callback ):
  connection = kombu.Connection( config[ 'MQ_DSN' ], connect_timeout = config[ 'MQ_CONNECT_TIMEOUT' ] )
  exchange = kombu.Exchange( config[ 'MQ_EXCHANGE_NAME' ], type = config[ 'MQ_EXCHANGE_TYPE' ] )
  queue = kombu.Queue( name = config[ 'MQ_QUEUE_NAME' ], exchange = exchange, routing_key = config[ 'MQ_QUEUE_ROUTING_KEY' ] )
  consumer = kombu.Consumer( connection, queues = queue, callbacks = [ callback ], accept = [ 'text/plain' ] )
  consumer.consume()
  return connection


def main():
  logging.info( 'Booting drain server' )

  logging.info( 'Loading configuration' )
  config = get_config()
  logging.debug( 'Configuration: {}'.format( config ) )

  logging.info( 'Creating InfluxDB client instance' )
  influx = get_influx( config )

  DRAIN_TIMEOUT = config[ 'MQ_DRAIN_TIMEOUT' ]
  ACK = config[ 'MQ_ACK' ]
  INFLUX_DB = config[ 'INFLUX_DB' ]

  def handle_message( body, message ):
    logging.info( 'Handling message #{}'.format( message.delivery_tag ) )
    logging.debug( 'Received message queue message {} with body: {}'.format( message, body ) )
    influx.write( body.splitlines(), params = { 'db': INFLUX_DB }, protocol = 'line' )
    if ACK:
      message.ack()

  logging.info( 'Creating AMQP client instance' )
  amqp = get_amqp( config, handle_message )

  try:
    logging.info( 'Drain server booted' )
    logging.info( 'Handling AMQP messages' )
    while True:
      try:
        amqp.drain_events( timeout = DRAIN_TIMEOUT )
      except socket.timeout:
        pass
  except KeyboardInterrupt:
    logging.info( 'Received SIGINT' )

  logging.info( 'Shutting down drain server' )
  amqp.release()

  logging.info( 'Drain server shut down' )


if __name__ == '__main__':
  configure_logging()
  main()
